const express = require ('express');
const app = express();
const stripe = require("stripe")("sk_test_1sIUTf3y7DJCrXalj6yo4rSM");

app.use(require("body-parser").text());


app.post("/charge", async (req, res) => {
  try {
    let {status} = await stripe.charges.create({
      amount: 250000,
      currency: "eur",
      description: "An example charge",
      source: req.body
    });

    res.json({status});
  } catch (err) {
    res.status(500).end();
  }
});

const port = 5000;

app.listen(port, () => console.log('server started on port ${port}'));
